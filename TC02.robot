*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC02-01
    Search Rooms TC02-01
    Close Browser

TC02-02
    Open Jetzt Mieten
    Click Button    SUCHEN
    Location Should Contain    https://96rooms.de/register-step1.php
    Wait Until Page Contains    MIETEN
    Click Button    //*[@id="tab1"]/div/div/div/div[2]/div/div[4]/div/div/form/div[3]/button
    Location Should Contain    https://96rooms.de/register-step2.php
    Close Browser
