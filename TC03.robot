*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC03-01
    Search Rooms TC02-01
    Input Text    member_name    tsgsdgasdg
    Input Text    member_surname    asfasfqfwfqwf
    Input Text    //*[@id="tab2"]/div/div/div/div[1]/form/div[3]/div/input    hgdghgh@asfasf.com
    Input Password    //*[@id="tab2"]/div/div/div/div[1]/form/div[4]/div/input    124124124
    Input Password    member_pass2    124124124
    Select Checkbox    term_confirm
    Close Browser

TC03-02
    Search Rooms TC02-01
    Select Checkbox    term_confirm
    Click Button    register-submit
    Location Should Contain    https://96rooms.de/register-step2.php
    Close Browser
