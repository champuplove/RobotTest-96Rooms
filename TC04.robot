*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC04-01
    Search Rooms TC02-01
    Input Text    //*[@id="tab2"]/div/div/div/div[2]/form/div[1]/div/input    test@test.com
    Input Password    //*[@id="tab2"]/div/div/div/div[2]/form/div[2]/div/input    124125
    Close Browser

TC04-02
    Search Rooms TC02-01
    Click Button    Anmelden
    Location Should Be    https://96rooms.de/register-step2.php
    Close Browser

TC04-03
    Search Rooms TC02-01
    Input Text    //*[@id="tab2"]/div/div/div/div[2]/form/div[1]/div/input    aek@gmail.com
    Click Button    Anmelden
    Location Should Be    https://96rooms.de/register-step2.php
    Close Browser

TC04-04
    Search Rooms TC02-01
    Input Password    //*[@id="tab2"]/div/div/div/div[2]/form/div[2]/div/input    1234
    Click Button    Anmelden
    Location Should Be    https://96rooms.de/register-step2.php
    Close Browser

TC04-05
    Search Rooms TC02-01
    Input Text    //*[@id="tab2"]/div/div/div/div[2]/form/div[1]/div/input    sdfda@asfasf.com
    Input Password    //*[@id="tab2"]/div/div/div/div[2]/form/div[2]/div/input    sfffsfsf
    Click Button    Anmelden
    Alert Should Be Present    Email or Password incorrect
    Choose Ok On Next Confirmation
    Close Browser