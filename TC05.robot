*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC05-01
    Open Rooms
    Click Link    //*[@id="accordion"]/li[1]/ul/li[1]/a
    Location Should Contain    https://96rooms.de/room.php?set%3D1
    Wait Until Page Contains    ROOM Typ A
    Close Browser

TC05-02
    Open Rooms
    Click Link    //*[@id="accordion"]/li[1]/ul/li[2]/a
    Location Should Contain    https://96rooms.de/room.php?set%3D2
    Wait Until Page Contains    ROOM Typ B
    Close Browser

TC05-03
    Open Rooms
    Click Link    //*[@id="accordion"]/li[1]/ul/li[3]/a
    Location Should Contain    https://96rooms.de/room.php?set%3D3
    Wait Until Page Contains    ROOM Typ C
    Close Browser

*** Keywords ***
Open Rooms
    Click Element    //*[@id="accordion"]/li[1]/div
