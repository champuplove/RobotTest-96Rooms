*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC06-01
    Open Schwarzes Brett
    Input Text    member_email    test@test.com
    Input Password    member_pass1    12345
    Close Browser

TC06-02
    Open Schwarzes Brett
    Click Button    ANMELDEN
    Close Browser

TC06-03
    Open Schwarzes Brett
    Input Text    member_email    test@test.com
    Click Button    ANMELDEN
    Close Browser

TC06-04
    Open Schwarzes Brett
    Input Text    member_pass1    12345
    Click Button    ANMELDEN
    Close Browser

TC06-05
    Open Schwarzes Brett
    Input Text    member_email    Test@test.com
    Input Password    member_pass1    123124124
    Click Button    ANMELDEN
    Close Browser

*** Keywords ***
Open Schwarzes Brett
    Click Link    //*[@id="accordion"]/li[2]/div/a
    Location Should Contain    https://96rooms.de/login.php
    Wait Until Page Contains    Schwarzes Brett
