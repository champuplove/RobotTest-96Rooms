*** Settings ***
Resource          common.robot
Test Setup        Open Home Page

*** Test Cases ***
TC07-01
    Open Hello Nachbarschaft
    Click Element    //*[@id="accordion"]/li[3]/ul/li[1]/a
    Location Should Contain    https://96rooms.de/map.php?set%3D1
    Wait Until Page Contains    DEINE NACHBARSCHAFT
    Click Element    //*[@id="accordion3"]/div[1]/div[1]/h4/a
    Click Link    map-1
    Close Browser

TC07-02
    Open Hello Nachbarschaft
    Set Selenium Speed    1
    Click Element    //*[@id="accordion"]/li[3]/ul/li[2]/a
    Location Should Contain    https://96rooms.de/map.php?set%3D2
    Wait Until Page Contains    DEIN HAMBURG
    Click Link    mapham-2
    Close Browser

*** Keywords ***
Open Hello Nachbarschaft
    Click Link    //*[@id="accordion"]/li[3]/div/a
