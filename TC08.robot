*** Settings ***
Resource          common.robot
Test Setup        Open Home Page
Test Teardown     Close Browser

*** Test Cases ***
TC08-01
    Click Link    //*[@id="accordion"]/li[4]/div/a
    Location Should Contain    https://96rooms.de/faq.php
    Wait Until Page Contains    FAQ’s
    Click Element    //*[@id="accordion2"]/div[14]/div[1]/h4/a
