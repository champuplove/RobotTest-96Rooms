*** Settings ***
Resource          common.robot
Test Setup        Open Home Page
Test Teardown     Close Browser

*** Test Cases ***
TC09-01
    Click Link    //*[@id="nav-top"]/div[2]/ul/li/a
    Select Window    url=https://www.facebook.com/96ROOMS/
    Location Should Contain    https://www.facebook.com/96ROOMS/
    Wait Until Page Contains    Facebook