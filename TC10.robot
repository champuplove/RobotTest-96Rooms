*** Settings ***
Resource          common.robot
Test Setup        Open Home Page
Test Teardown     Close Browser

*** Test Cases ***
TC10-01
    Click Link    //*[@id="nav-bottom"]/div/ul/li[1]/a
    Location Should Contain    https://96rooms.de/impressum.php
    Wait Until Page Contains    Impressum