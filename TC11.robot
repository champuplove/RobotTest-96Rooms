*** Settings ***
Resource          common.robot
Test Setup        Open Home Page
Test Teardown     Close Browser

*** Test Cases ***
TC11-01
    Click Link    //*[@id="nav-bottom"]/div/ul/li[2]/a
    Location Should Contain    https://96rooms.de/data.php
    Wait Until Page Contains    Datenschutzerklärung
