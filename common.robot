*** Settings ***
Library           Selenium2Library

*** Variables ***
${Home}    https://96rooms.de/
${Browser}    gc

*** Keywords ***
Open Home Page
    Open Browser    ${Home}    ${Browser}
    Location Should Contain    https://96rooms.de/
    Wait Until Page Contains    96ROOMS

Open Jetzt Mieten
    Click Link    //*[@id="nav-top"]/div[1]/div/a
    Location Should Contain    https://96rooms.de/register-step1.php

Search Rooms TC02-01
    Open Jetzt Mieten
    Select From List    search_roomtype
    Select From List    search_roomtype    all
    Click Element    datepicker
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[5]/a
    Set Selenium Speed    1
    Select From List    search_roomrate    526-780
    Click Button    SUCHEN
    Location Should Contain    https://96rooms.de/register-step1.php
    Wait Until Page Contains    MIETEN
    Click Button    //*[@id="tab1"]/div/div/div/div[2]/div/div[4]/div/div/form/div[3]/button
    Location Should Contain    https://96rooms.de/register-step2.php
